#ifndef TRANSITSORTFILTERPROXYMODEL_H
#define TRANSITSORTFILTERPROXYMODEL_H
#include <QSortFilterProxyModel>

class TransitSortFilterProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    TransitSortFilterProxyModel(QObject *parent = Q_NULLPTR);
    bool lessThan(const QModelIndex &left, const QModelIndex &right) const;
};

#endif // TRANSITSORTFILTERPROXYMODEL_H
