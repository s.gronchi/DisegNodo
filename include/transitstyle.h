#ifndef TRANSITSTYLE_H
#define TRANSITSTYLE_H

#include <QBrush>
#include <QFont>
#include <QPen>

class TransitStyle
{
    QFont m_font;
    QBrush m_fgBrush;
    QPen m_pen;
    QBrush m_bgBrush;

public:
    TransitStyle();

    QFont font() const;
    void setFont(const QFont &font);
    QBrush foregroundBrush() const;
    void setForegroundBrush(const QBrush &fgBrush);
    QPen pen() const;
    void setPen(const QPen &pen);
    QBrush backgroundBrush() const;
    void setBackgroundBrush(const QBrush &bgBrush);
};

#endif // TRANSITSTYLE_H
