#include "transit.h"
#include <exception>
#include <string>

Transit::Transit(): m_minute(-1)
{ }

Transit::Transit(QString from, QString to, int minute): m_from(from), m_to(to)
{
    if ((minute < 0) || (minute > 59))
        throw std::out_of_range("Minute must be between 0 and 59, was " + std::to_string(minute));
    m_minute = minute;
}

bool Transit::operator==(Transit rhs)
{
    return (m_minute == rhs.m_minute)
        && (m_from == rhs.m_from) // QString is implicitly shared, so this is cheap
        && (m_to == rhs.m_to);
}

const QString &Transit::from() const
{
    return m_from;
}

const QString &Transit::to() const
{
    return m_to;
}

int Transit::minute() const
{
    return  m_minute;
}

void Transit::setMinute(int newMinute)
{
    if ((newMinute < 0) || (newMinute > 59))
        throw std::out_of_range("Minute must be between 0 and 59, was " + std::to_string(newMinute));
    m_minute = newMinute;
}

void Transit::setFrom(QString newFrom)
{
    m_from = newFrom;
}

void Transit::setTo(QString newTo)
{
    m_to = newTo;
}
